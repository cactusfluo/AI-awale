#!/usr/bin/python3
# Filename: [ga.py]

################################################################################
##[ IMPORTS ]###################################################################
################################################################################

import random;
import math;

################################################################################
###[ MODULE ]###################################################################
################################################################################

##################################################
class   c_ga:
    """
        Main genetic algorithm class.

        Properties:
        .birth_func -> function to call to produce a new element from two elements.
        .selection_mode -> mode of selection RANK-WHEEL-TOURNAMENT-RANDOM, the
            default mode is WHEEL.
        .mutations -> list of mutations that can be calculated during birth
            period.
        .population -> main population.
    """
    ##################################################
    ##[ PROPERTIES ]##################################
    ##################################################
    birth_func     = None;
    selection_mode = "WHEEL";
    mutations   = [];
    population  = [];

    ##################################################
    ##[ METHODS ]#####################################
    ##################################################

    ############################## 
    ##[ SET PROPERTIES ]##########
    ##############################

    ##############################
    def set_population(self, p_new):
        self.population = p_new;

    ##############################
    def set_birth_function(self, new_func):
        self.birth_func = new_func;

    ##############################
    def set_selection_mode(self, new_mode):
        self.selection_mode = new_mode;

    ############################## 
    ##[ MANAGE ]##################
    ##############################

    ##############################
    def add_mutation(self, func, chance):
        self.mutations.append({"func": func, "chance": chance});

    ##############################
    def select(self, ratio):
        ##############################
        ##[ STATIC FUNCTIONS ]########
        ##############################

        ##############################
        def select_wheel(self, num_death):
            wheel = [];
            num_keep = len(self.population) - num_death;
            new_population = [];
            min = self.population[-1]["note"];
            i = 0;
            for i in range(0, len(self.population)):
                for _ in range(0, (self.population[i]["note"] - min) + 1):
                    wheel.append(i);
            for _ in range(0, num_keep):
                rand = math.floor(random.uniform(0, len(wheel)));
                value = wheel[rand];
                new_population.append(self.population[value]);
                while (value in wheel):
                    wheel.remove(value);
            self.population = new_population;

        ##############################
        def select_rank(self, num_death):
            for _ in range(0, num_death):
                del self.population[-1];

        ##############################
        def select_tournament(self, num_death):
            list = [0, 0, 0];
            for _ in range(0, num_death):
                list[0] = math.floor(random.uniform(0, len(self.population)));
                list[1] = math.floor(random.uniform(0, len(self.population)));
                list[2] = math.floor(random.uniform(0, len(self.population)));
                list.sort(key=lambda x: self.population[x]["note"]);
                del self.population[list[0]];

        ##############################
        def select_random(self, num_death):
            for _ in range(0, num_death):
                rand = math.floor(random.uniform(0, len(self.population)));
                del self.population[rand];

        ##############################
        def mutate(self, elem):
            for mutation in self.mutations:
                chance = math.floor(random.uniform(0, mutation["chance"]));
                if (chance == 0):
                    mutation["func"](elem);

        ##############################
        ##[ MAIN ]####################
        ##############################
        select_func = {
                        "RANK": select_rank,
                        "WHEEL": select_wheel,
                        "TOURNAMENT": select_tournament,
                        "RANDOM": select_random
                        };
        p_length = len(self.population);
        num_death = round((p_length * ratio) // 100);
        self.population.sort(reverse=True, key=lambda elem: elem["note"]);
        select_func[self.selection_mode](self, num_death);
        new_elements = [];
        for _ in range(0, num_death):
            rand = math.floor(random.uniform(0, len(self.population)))
            elem_a = self.population[rand];
            rand = math.floor(random.uniform(0, len(self.population)))
            elem_b = self.population[rand];
            elem_new = self.birth_func(elem_a, elem_b);
            mutate(self, elem_new);
            new_elements.append(elem_new);
        self.population += new_elements;

    ############################## 
    ##[ OTHER ]###################
    ##############################

    ##############################
    def get_best_elements(self, num):
        self.population.sort(reverse=True, key=lambda elem: elem["note"]);
        if (num):
            return (self.population[0:num]);
        return (self.population[0]);

    ##############################
    def print_population(self):
        print(self.population);

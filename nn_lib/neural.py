#!/usr/bin/python3

################################################################################
##[ IMPORTS ]###################################################################
################################################################################

import  json;
import  random;
import  math;

################################################################################
##[ CLASS ]#####################################################################
################################################################################

##################################################
class c_nn:
    """
        This is the main neural network class.
        There are two types of initialisation:
            . [init_new] which requires a matrix, a list of layers length.
            . [init_from_file] which requires the file name.
    """
    ##############################################
    ##[ PROPERTIES ]##############################
    ##############################################
    net = [];

    ##############################################
    ##[ METHODS ]#################################
    ##############################################

    ##############################
    ##[ INIT ]####################
    ##############################

    ##############################
    def init(self, layers):
        self.net = [];
        ##### FOR EACH LAYER #####
        for layer in range(len(layers)):
            new_layer = [];
            ##### FOR EACH NEURON #####
            for neuron in range(layers[layer]):
                new_neuron = {"value": 0};
                if (layer > 0): 
                    weights = [];
                    new_neuron["bias"] = random.uniform(-5, 5);
                    new_neuron["weights"] = weights;
                    ##### FOR EACH WEIGHT #####
                    for weight in range(layers[layer - 1]):
                        weights.append(random.uniform(-3, 3));
                new_layer.append(new_neuron);
            self.net.append(new_layer);  

    ##############################
    def init_from_file(self, name):
        file = open(name, "r");
        string = file.read();
        file.close();
        nn = json.loads(string);
        ##### FOR EACH LAYER #####
        for layer in nn:
            ##### FOR EACH NEURON #####
            for neuron in layer:
                neuron["value"] = 0;
        self.net = nn;

    ##############################
    def init_from_weights(self, nn):
        ##### FOR EACH LAYER #####
        for layer in nn:
            ##### FOR EACH NEURON #####
            for neuron in layer:
                neuron["value"] = 0;
        self.net = nn;

    ##############################
    ##[ OTHER FUNCTIONS ]#########
    ##############################

    ##############################
    def get_net(self):
        return (self.net);

    ##############################
    def export(self, name):
        for layer in self.net:
            for neuron in layer:
                del neuron["value"];
        string = json.dumps(self.net, separators=(',',':'));
        file = open(name + ".nno", "w");
        file.write(string);
        file.close();
        for layer in self.net:
            for neuron in layer:
                neuron["value"] = 0;

    ##############################
    def print_values(self):
        i = 0;
        max_layer = len(self.net);
        while (i < max_layer):
            j = 0;
            max_neuron = len(self.net[i]);
            print("LAYER [" + str(i + 1) + "]");
            while (j < max_neuron):
                print("(" + str(self.net[i][j]["value"]) + ")");
                j += 1;
            i += 1;


    ##############################
    ##[ NN FUNCTIONS ]############
    ##############################

    ##############################
    def launch(self, input):
        ###############
        ##### SET INPUT
        ###############
        i = 0;
        for neuron in self.net[0]:
            neuron["value"] = input[i];
            i += 1;
        ###############
        ##### LAUNCH
        ###############
        i = 1;
        max_layer = len(self.net);
        ##### FOR EACH LAYER #####
        while (i < max_layer):
            ##### FOR EACH NEURON #####
            for neuron in self.net[i]:
                input = neuron["bias"];
                j = 0;
                weights = neuron["weights"];
                max_weight = len(weights);
                ##### FOR EACH WEIGHT #####
                while (j < max_weight):
                    value = self.net[i - 1][j]["value"];
                    weight = weights[j];
                    input += value * weight;
                    j += 1;
                neuron["value"] = 1 / (1 + math.exp(-input));
            i += 1;
        ###############
        ##### OUTPUT
        ###############
        i = 0;
        output_layer = self.net[-1];
        output = [];
        for neuron in self.net[-1]:
            output.append(neuron["value"]);
        return (output);

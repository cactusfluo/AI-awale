#!/usr/bin/python3

import sys;
from nn_lib     import neural as nn;
from aw_lib     import awale;

################################################################################
##[ FUNCTIONS ]#################################################################
################################################################################

##############################
def sort_move(output):
    sorted = [];
    for i in range(0, 6):
        sorted.append({"order": i, "value": output[i]});
    sorted.sort(key=lambda x: x["value"], reverse=True);
    return (sorted);

################################################################################
##[ MAIN ]######################################################################
################################################################################

##############################
##[ INIT ]####################
##############################
if (len(sys.argv) != 2):
    print("ERROR: Wrong number of arguments");
    exit(1);
##### INIT MODULES #####
game = awale.c_awale();
game.init();
##### INIT AI #####
ai = nn.c_nn();
ai.init_from_file(sys.argv[1]);

##############################
##[ LAUNCH ]##################
##############################

state = 0;
while (True):
    game.print_board();
    ###############
    ##### TURN (PLAYER) #####
    ###############
    if (game.get_turn() == 1):
        move = "NULL";
        while (move.isdigit() == False or int(move) > 6 or int(move) < 1):
            move = input("next movement: ");
        state = game.play(int(move));
    ###############
    ##### TURN (AI) #####
    ###############
    else:
        board = game.get_board(2);
        output = ai.launch(board);
        sorted = sort_move(output);
        for i in range(0, 6):
            state = game.play(sorted[i]["order"] + 1);
            if (state == 0 or state == 2):
                print("AI played: " + str(sorted[i]["order"] + 1));
                break;
    if (state == 2):
        print("GAME OVER");
        print("Player [" + str(game.get_winner()) + "] has won !");
        break;
